#include <napi.h>

#include <iostream>
#include <vector>
#include <string>
#include <locale>
#include <codecvt>

#include <Windows.h>
#include <iwscapi.h>
#include <wscapi.h>

#include "SecurityProduct.h"

using namespace std;
using convert_type = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_type, wchar_t> converter;

vector<SecurityProduct*>* GetSecurityProducts(WSC_SECURITY_PROVIDER type) {
	// Create COM instance for productList
	IWSCProductList* productList = nullptr;
	HRESULT result = CoCreateInstance(
		__uuidof(WSCProductList), NULL, CLSCTX_INPROC_SERVER,
		__uuidof(IWSCProductList), reinterpret_cast<LPVOID*> (&productList)
	);

	if (result != S_OK) {
		return nullptr;
	}

	result = productList->Initialize(type);
	if (result != S_OK) {
		return nullptr;
	}

	LONG count = 0;
	result = productList->get_Count(&count);
	if (result != S_OK) {
		return nullptr;
	}

	auto products = new vector<SecurityProduct*>();
	IWscProduct* product = nullptr;
	for (LONG i = 0; i < count; i++)
	{
		result = productList->get_Item(i, &product);
		if (result != S_OK) {
			delete products;
			return nullptr;
		}

		products->push_back(SecurityProduct::fromWscProduct(type, product));
		product->Release();
		product = nullptr;
	}

	if (productList != NULL)
	{
		productList->Release();
	}

	return products;
}

// Converts wstring to utf string and returns napi utf8 string value
Napi::String ValueFromWString(Napi::Env env, wstring* str) {
	return Napi::String::New(env, converter.to_bytes(*str));
}

// Builds napi array of napi objects from security products ptrs vector
Napi::Array SecurityProductsVectorToArray(Napi::Env env, vector<SecurityProduct*>* vector) {
	Napi::Array result = Napi::Array::New(env);
	
	if (vector == nullptr) {
		return result;
	}

	int index = 0;
	for (auto iter = vector->begin(); iter != vector->end(); iter++) {
		SecurityProduct* product = *iter;
		Napi::Object obj = Napi::Object::New(env);

		// Set object fields
		obj.Set("name", ValueFromWString(env, product->getName()));
		obj.Set("type", (int)product->getType());
		obj.Set("state", (int)product->getState());

		if (product->getType() != WSC_SECURITY_PROVIDER_FIREWALL) {
			obj.Set("sigStatus", (int)product->getSignatureStatus());
		}

		obj.Set("remediationPath", ValueFromWString(env, product->getRemediationPath()));

		if (product->getType() == WSC_SECURITY_PROVIDER_ANTIVIRUS) {
			obj.Set("stateTimestamp", ValueFromWString(env, product->getStateTimestamp()));
		}

		// Add object to array
		result.Set(index++, obj);
	}
	return result;
}


Napi::Object Method(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	auto antiviruses = GetSecurityProducts(WSC_SECURITY_PROVIDER_ANTIVIRUS);
	auto firewalls = GetSecurityProducts(WSC_SECURITY_PROVIDER_FIREWALL);
	auto antispyware = GetSecurityProducts(WSC_SECURITY_PROVIDER_ANTISPYWARE);
	
	auto avArray = SecurityProductsVectorToArray(env, antiviruses);
	auto fwArray = SecurityProductsVectorToArray(env, firewalls);
	auto aswArray = SecurityProductsVectorToArray(env, antispyware);

	Napi::Object obj = Napi::Object::New(env);
	obj.Set("antivirus", avArray);
	obj.Set("firewall", fwArray);
	obj.Set("antispyware", aswArray);

	return obj;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	// Initialize COM
	HRESULT result = CoInitializeEx(0, COINIT_APARTMENTTHREADED);
	if (result != S_OK) {
		// TODO Return "Initialization failed" somehow
	}

	exports.Set(
		Napi::String::New(env, "getSecurityProducts"),
		Napi::Function::New(env, Method)
	);
	return exports;
}

NODE_API_MODULE(hello, Init)
