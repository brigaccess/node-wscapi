#pragma once
#include <string>

#include <Windows.h>
#include <iwscapi.h>
#include <wscapi.h>

using namespace std;

enum SECURITY_SIGNATURE_STATUS {
	UNAPPLICABLE = -1,
	SECURITY_PRODUCT_OUT_OF_DATE = WSC_SECURITY_PRODUCT_OUT_OF_DATE,
	SECURITY_PRODUCT_UP_TO_DATE = WSC_SECURITY_PRODUCT_UP_TO_DATE
};

class SecurityProduct {
protected:
	// Security product name
	wstring* name = nullptr;
	// Security product type
	WSC_SECURITY_PROVIDER type = WSC_SECURITY_PROVIDER_ANTIVIRUS;
	// Security product state
	WSC_SECURITY_PRODUCT_STATE state = WSC_SECURITY_PRODUCT_STATE_EXPIRED;
	// Security product signature status. Defined for everything except firewalls.
	WSC_SECURITY_SIGNATURE_STATUS sigStatus = WSC_SECURITY_PRODUCT_OUT_OF_DATE;
	// Security product remediation path.
	wstring* remediationPath = nullptr;
	// State timestamp. Defined for antiviruses only.
	wstring* stateTimestamp = nullptr;

	SecurityProduct() {}
public:
	/*
	 * Builds SecurityProduct from IWscProduct if possible. Otherwise, returns NULL.
	 * Note that this method does not release product so you have to do it on the caller side.
	 */
	static SecurityProduct* fromWscProduct(WSC_SECURITY_PROVIDER type, IWscProduct* product);
	wstring* const getName();
	WSC_SECURITY_PROVIDER getType();
	WSC_SECURITY_PRODUCT_STATE getState();
	// Returns UNAPPLICABLE for firewalls
	SECURITY_SIGNATURE_STATUS getSignatureStatus();
	wstring* const getRemediationPath();
	wstring* const getStateTimestamp();
	~SecurityProduct();
};