#include "SecurityProduct.h"

SecurityProduct* SecurityProduct::fromWscProduct(WSC_SECURITY_PROVIDER type, IWscProduct* product) {
	SecurityProduct* instance = new SecurityProduct();
	BSTR strptr;
	HRESULT result;

	// Set type
	instance->type = type;

	// Set name
	result = product->get_ProductName(&strptr);
	if (result != S_OK) {
		goto error;
	}
	instance->name = new wstring(strptr);
	SysFreeString(strptr);

	// Set product state
	WSC_SECURITY_PRODUCT_STATE state;
	result = product->get_ProductState(&state);
	if (result != S_OK) {
		goto error;
	}
	instance->state = state;

	// Set product state timestamp for antivirus
	if (type == WSC_SECURITY_PROVIDER_ANTIVIRUS) {
		result = product->get_ProductStateTimestamp(&strptr);
		if (result != S_OK) {
			goto error;
		}
		instance->stateTimestamp = new wstring(strptr);
		SysFreeString(strptr);
	}

	// Set product signature status if not firewall
	if (type != WSC_SECURITY_PROVIDER_FIREWALL) {
		WSC_SECURITY_SIGNATURE_STATUS status;
		result = product->get_SignatureStatus(&status);
		if (result != S_OK) {
			goto error;
		}
		instance->sigStatus = status;
	}

	// Set remediation path
	result = product->get_RemediationPath(&strptr);
	if (result != S_OK) {
		goto error;
	}
	instance->remediationPath = new wstring(strptr);
	SysFreeString(strptr);

	goto success;
error:
	delete instance;
	return NULL;
success:
	return instance;
}

wstring* const SecurityProduct::getName() {
	return this->name;
}

WSC_SECURITY_PROVIDER SecurityProduct::getType() {
	return this->type;
}

WSC_SECURITY_PRODUCT_STATE SecurityProduct::getState() {
	return this->state;
}

// Returns UNAPPLICABLE for firewalls
SECURITY_SIGNATURE_STATUS SecurityProduct::getSignatureStatus() {
	if (this->type == WSC_SECURITY_PROVIDER_FIREWALL) {
		return UNAPPLICABLE;
	}
	return (SECURITY_SIGNATURE_STATUS)this->sigStatus;
}

wstring* const SecurityProduct::getRemediationPath() {
	return this->remediationPath;
}

wstring* const SecurityProduct::getStateTimestamp() {
	return this->stateTimestamp;
}

SecurityProduct::~SecurityProduct() {
	delete this->name;
	delete this->remediationPath;
	delete this->stateTimestamp;
}