const node_wscapi = require('bindings')('node-wscapi');
exports.getSecurityProducts = node_wscapi.getSecurityProducts;

// https://docs.microsoft.com/en-us/windows/desktop/api/iwscapi/ne-iwscapi-wsc_security_product_state
exports.PRODUCT_ON = 0;
exports.PRODUCT_OFF = 1;
exports.PRODUCT_SNOOZED = 2;
exports.PRODUCT_EXPIRED = 3;

// https://docs.microsoft.com/en-us/windows/desktop/api/iwscapi/ne-iwscapi-wsc_security_signature_status
exports.PRODUCT_OUT_OF_DATE = 0;
exports.PRODUCT_UP_TO_DATE = 1;
